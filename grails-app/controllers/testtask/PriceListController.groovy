package testtask


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PriceListController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PriceList.list(params), model: [priceListInstanceCount: PriceList.count()]
    }

    def show(PriceList priceListInstance) {
        respond priceListInstance
    }

    def create = {
        respond new PriceList(params)
    }
    /*
    *
    * изменение дат
    *
    * */

    def changeDates(PriceList priceList) {
        if (PriceList.findByCodeOfProduct(priceList.codeOfProduct)) {

            def price = PriceList.findAllByCodeOfProduct(priceList.codeOfProduct).toSorted(new PriceListCompare())

            for (it in price) {
                if (it.beginDate >= priceList.beginDate && it.endDate <= priceList.endDate) {
                    PriceList deletedItem = PriceList.findById(it.id)
                    deletedItem.delete();
                    continue
                }

                if (it.beginDate <= priceList.beginDate && it.endDate >= priceList.endDate) {
                    PriceList item = PriceList.findById(it.id)
                    new PriceList(codeOfProduct: item.codeOfProduct, beginDate: priceList.endDate.plus(1), endDate: item.endDate, price: item.price).save(flush: true, failOnError: true)
                    item.endDate = priceList.beginDate.minus(1)
                    item.save()
                    break
                }

                if (it.beginDate >= priceList.beginDate && it.endDate >= priceList.endDate && it.beginDate <= priceList.endDate) {
                    PriceList item = PriceList.findById(it.id)
                    item.beginDate = priceList.endDate.plus(1)
                    item.save()
                }

                if (it.beginDate <= priceList.beginDate && it.endDate <= priceList.endDate && it.endDate >= priceList.beginDate) {
                    PriceList item = PriceList.findById(it.id)
                    item.endDate = priceList.beginDate.minus(1)
                    item.save(flush: true, failOnError: true)
                }
            }
            priceList.save flush: true
            redirect action: 'index'

        } else {
            priceList.save flush: true
            redirect priceList
        }
    }

    def edit(PriceList priceListInstance) {
        respond priceListInstance
    }

    @Transactional
    def delete(PriceList priceListInstance) {

        if (priceListInstance == null) {
            notFound()
            return
        }

        priceListInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PriceList.label', default: 'PriceList'), priceListInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'priceList.label', default: 'PriceList'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
