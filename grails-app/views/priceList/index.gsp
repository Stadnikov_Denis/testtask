
<%@ page import="testtask.PriceList" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'priceList.label', default: 'Цены')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-priceList" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}">На главную</a></li>
				<li><g:link class="create" action="create">Создать товар с временными ограничениями</g:link></li>
			</ul>
		</div>
		<div id="list-priceList" class="content scaffold-list" role="main">
			<h1>Список товаров</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			${res}
			<table>
			<thead>
					<tr>
						<g:sortableColumn property="codeOfProduct" title="${message(code: 'priceList.codeOfProduct.label', default: 'Код товара')}" />
					
						<g:sortableColumn property="beginDate" title="${message(code: 'priceList.beginDate.label', default: 'Начало действия цены')}" />
					
						<g:sortableColumn property="endDate" title="${message(code: 'priceList.endDate.label', default: 'Конец действия цены')}" />
					
						<g:sortableColumn property="price" title="${message(code: 'priceList.price.label', default: 'Цена')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${priceListInstanceList}" status="i" var="priceListInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td>${fieldValue(bean: priceListInstance, field: "codeOfProduct")}</td>

						<td><g:link action="show" id="${priceListInstance.id}">${fieldValue(bean: priceListInstance, field: "beginDate")}</g:link></td>
					
						<td><g:formatDate date="${priceListInstance.endDate}" /></td>
					
						<td>${fieldValue(bean: priceListInstance, field: "price")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${priceListInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
