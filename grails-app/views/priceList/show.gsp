
<%@ page import="testtask.PriceList" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'priceList.label', default: 'PriceList')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-priceList" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-priceList" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list priceList">
			
				<g:if test="${priceListInstance?.beginDate}">
				<li class="fieldcontain">
					<span id="beginDate-label" class="property-label"><g:message code="priceList.beginDate.label" default="Begin Date" /></span>
					
						<span class="property-value" aria-labelledby="beginDate-label"><g:formatDate date="${priceListInstance?.beginDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceListInstance?.codeOfProduct}">
				<li class="fieldcontain">
					<span id="codeOfProduct-label" class="property-label"><g:message code="priceList.codeOfProduct.label" default="Code Of Product" /></span>
					
						<span class="property-value" aria-labelledby="codeOfProduct-label"><g:fieldValue bean="${priceListInstance}" field="codeOfProduct"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceListInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="priceList.endDate.label" default="End Date" /></span>
					
						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${priceListInstance?.endDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceListInstance?.price}">
				<li class="fieldcontain">
					<span id="price-label" class="property-label"><g:message code="priceList.price.label" default="Price" /></span>
					
						<span class="property-value" aria-labelledby="price-label"><g:fieldValue bean="${priceListInstance}" field="price"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:priceListInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">

					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
