<%@ page import="testtask.PriceList" %>



<div class="fieldcontain ${hasErrors(bean: priceListInstance, field: 'beginDate', 'error')} required">
	<label for="beginDate">
		<g:message code="priceList.beginDate.label" default="Begin Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="beginDate" precision="day"  value="${priceListInstance?.beginDate}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: priceListInstance, field: 'codeOfProduct', 'error')} required">
	<label for="codeOfProduct">
		<g:message code="priceList.codeOfProduct.label" default="Code Of Product" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="codeOfProduct" type="number" value="${priceListInstance.codeOfProduct}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: priceListInstance, field: 'endDate', 'error')} required">
	<label for="endDate">
		<g:message code="priceList.endDate.label" default="End Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="endDate" precision="day"  value="${priceListInstance?.endDate}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: priceListInstance, field: 'price', 'error')} required">
	<label for="price">
		<g:message code="priceList.price.label" default="Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="price" type="number" value="${priceListInstance.price}" required=""/>

</div>

